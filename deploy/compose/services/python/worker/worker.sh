#!/bin/bash
mkdir -p /var/log/celery/
touch /var/log/celery/worker.log
celery worker -A errbody -c 1 -l info
