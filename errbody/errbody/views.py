from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect
from django.utils.encoding import force_text
from django.views.generic.detail import (
    SingleObjectMixin, )
from django.views.generic.base import ContextMixin



class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class NeverCacheMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(NeverCacheMixin, cls).as_view(**initkwargs)
        return never_cache(view)


# class CacheMixin(object):
#     cache_timeout = 0
#
#     def get_cache_timeout(self):
#         return self.cache_timeout
#
#     def dispatch(self, *args, **kwargs):
#         return cache_page(self.get_cache_timeout())(super(CacheMixin, self).dispatch)(*args, **kwargs)


class PrivacyPolicyView(TemplateView):
    template_name = 'privacy_policy.html'


class TermsOfServiceView(TemplateView):
    template_name = 'terms_of_service.html'


class AboutUsView(TemplateView):
    template_name = 'about_us.html'


class FormSetMixin(ContextMixin):
    initial = {}
    formset_class = None
    success_url = None
    prefix = None

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        return self.initial.copy()

    def get_prefix(self):
        """
        Returns the prefix to use for forms on this view
        """
        return self.prefix

    def get_formset_class(self):
        """
        Returns the form class to use in this view
        """
        return self.formset_class

    def get_formset(self, formset_class=None):
        """
        Returns an instance of the form to be used in this view.
        """
        if formset_class is None:
            formset_class = self.get_formset_class()
        return formset_class(**self.get_formset_kwargs())

    def get_formset_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
        }

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_success_url(self):
        """
        Returns the supplied success URL.
        """
        if self.success_url:
            # Forcing possible reverse_lazy evaluation
            url = force_text(self.success_url)
        else:
            raise ImproperlyConfigured(
                "No URL to redirect to. Provide a success_url.")
        return url

    def formset_valid(self, formset):
        """
        If the form is valid, redirect to the supplied URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def formset_invalid(self, formset):
        """
        If the form is invalid, re-render the context data with the
        data-filled form and errors.
        """
        return self.render_to_response(self.get_context_data(formset=formset))


class InlineModelFormSetMixin(FormSetMixin, SingleObjectMixin):
    def get_formset_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(InlineModelFormSetMixin, self).get_formset_kwargs()
        if hasattr(self, 'object'):
            kwargs.update({'instance': self.object})
        return kwargs

    def formset_valid(self, formset):
        """
        If the form is valid, save the associated model.
        """
        self.object = formset.save()
        return super(InlineModelFormSetMixin, self).formset_valid(formset)


class InlineModelFormSetUpdateView(InlineModelFormSetMixin, TemplateView):
    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        self.object = self.get_object()
        formset = self.get_formset()
        return self.render_to_response(self.get_context_data(formset=formset))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        self.object = self.get_object()
        formset = self.get_formset()
        if formset.is_valid():
            return self.formset_valid(formset)
        else:
            return self.formset_invalid(formset)

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)


class IndexView(TemplateView):
    template_name = 'index.html'
